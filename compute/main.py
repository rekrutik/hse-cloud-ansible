import tornado
import tornado.ioloop
import tornado.web
import logging
import configargparse
import json
import socket
from netifaces import interfaces, ifaddresses, AF_INET

class AddressHandler(tornado.web.RequestHandler):
    def initialize(self, options):
        self.options = options

    async def get(self):
        hostname = socket.gethostname()
        self.write(json.dumps({'zxc': 'asd', 'hostname': hostname, 'address': ifaddresses(options.iface)}).encode(encoding='utf-8'))

def make_tornado(options):
    return tornado.web.Application([
        (r'/address', AddressHandler, {'options': options}),
    ])

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    p = configargparse.ArgParser(default_config_files=['config.ini'])
    p.add('-c', '--config', is_config_file=True, help='Config file path')
    p.add('--host', help='HTTP server host (it is likely not to be exposed without TLS)', default='0.0.0.0', env_var='HOST')
    p.add('--port', help='HTTP server port', default=80, type=int, env_var='PORT')
    p.add('--iface', help='Interface to get the address from', default='eth0', env_var='IFACE')
    options = p.parse_args()
    logger = logging.getLogger('compute')
    http_server = make_tornado(options)
    http_server.listen(options.port, address=options.host)
    tornado.ioloop.IOLoop.current().start()
